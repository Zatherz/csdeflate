﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using NMaier.GetOptNet;

namespace CSDeflate {
    [GetOptOptions(
        OnUnknownArgument = UnknownArgumentsAction.Throw,
        UsageIntro = "Usage: {{{PROGNAME}}} [--help|-h] --compress|--decompress INPUT [-o OUTPUT]"
    )]
    class Options : GetOpt {
        public string InputPath { get { return Parameters[0]; } }
        public CompressionMode? Mode = null;

        public Stream InputStream { get { return File.OpenRead(InputPath); } }
        public Stream OutputStream {
            get {
                if (OutputPath == "-") return Console.OpenStandardOutput();
                return File.OpenWrite(OutputPath);
            }
        }


        [Parameters(Min = 0, Max = 1)]
        public List<string> Parameters = new List<string>();

        [Argument("output", HelpText = "Set output path (single hyphen for stdout).")]
        [ShortArgument('o')]
        [FlagArgument(true)]
        public string OutputPath = "-";

        [Argument("decompress", HelpText = "Decompress input into output.")]
        [ShortArgument('d')]
        [FlagArgument(true)]
        public bool Deflate { set { if(value) Mode = CompressionMode.Decompress; } }

        [Argument("compress", HelpText = "Compress input into output.")]
        [ShortArgument('c')]
        [FlagArgument(true)]
        public bool Inflate { set { if(value) Mode = CompressionMode.Compress;} }

        [Argument("help", HelpText = "Show help.")]
        [ShortArgument('h')]
        [FlagArgument(true)]
        public bool HelpRequested;
    }

    class CLI {
        public static Options Options = new Options();

        private static string _usage = null;
        public static string Usage {
            get {
                if (_usage != null) return _usage;
                return _usage = Options.AssembleUsage(Console.WindowWidth).Replace("{{{PROGNAME}}}", AppDomain.CurrentDomain.FriendlyName);
            }
        }

        public static void PrintUsage() {
            Console.Write(Usage);
        }

        public static int Main(string[] args) {
            try { Options.Parse(args); } catch (GetOptException e) {
                Console.WriteLine(e.Message);
                PrintUsage();
                return 1;
            }

            if (Options.HelpRequested) {
                PrintUsage();
                return 0;
            }

            if (Options.Parameters.Count < 1) {
                PrintUsage();
                return 1;
            }

            if (Options.Mode == null) {
                PrintUsage();
                return 1;
            }

            if (Options.Mode == CompressionMode.Compress) {
                using (var input = Options.InputStream) {
                    using (var deflate = new DeflateStream(Options.OutputStream, Options.Mode.Value)) {
                        input.CopyTo(deflate);
                    }
                }
            } else {
                using (var output = Options.OutputStream) {
                    using (var deflate = new DeflateStream(Options.InputStream, Options.Mode.Value)) {
                        deflate.CopyTo(output);
                    }
                }
            }

            return 0;
        }
    }
}
