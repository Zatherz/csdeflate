# CSDeflate

Extremely simple commandline interface to the C# DeflateStream class (no headers/no checksums).

### Compress

    ./CSDeflate.exe -c CSDeflate.exe -o CSDeflate.zexe

### Decompress

    ./CSDeflate.exe -d CSDeflate.zexe -o CSDeflate.exe
